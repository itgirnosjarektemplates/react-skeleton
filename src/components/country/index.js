import React, {Component} from 'react';
import './index.css';

export default class Country extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryName: '',
            comment: '',
            ISO2: '',
            ISO3: '',
            lastEditUser: '',
            lastEditDate: '',
            isDeleted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        alert('Country was successfully created!');
        event.preventDefault();
    }

    deleteCountry(event) {
        alert('Country was successfully deleted!');
        event.preventDefault();
    }

    render() {
        return (
            <div className="country-parent">
                <div className="container country-content">
                    <div className="action-buttons">
                        <button type="button" className="btn btn-danger" onClick={this.deleteCountry}>Delete</button>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label>Name:</label>
                            <input className="form-control" type="text" value={this.state.countryName}
                                   onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>Comment:</label>
                            <textarea className="form-control" value={this.state.comment}
                                   onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>ISO2:</label>
                            <input className="form-control" type="text" value={this.state.ISO2}
                                   onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>ISO3:</label>
                            <input className="form-control" type="text" value={this.state.ISO3}
                                   onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>Last edited user:</label>
                            <input className="form-control" type="text" value={this.state.lastEditUser}
                                   onChange={this.handleChange} disabled={true} />
                        </div>
                        <div className="form-group">
                            <label>Last edit datetime:</label>
                            <input className="form-control" type="text" value={this.state.lastEditDate}
                                   onChange={this.handleChange} disabled={true} />
                        </div>
                        <div className="form-group">
                            <label>Is deleted:</label>
                            <input className="form-control" type="text" value={this.state.isDeleted}
                                   onChange={this.handleChange} disabled={true} />
                        </div>
                        <hr/>
                    </form>

                    <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </div>
            </div>
        );
    }
}